package com.example.catpawng;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class Paddle extends TextView implements View.OnTouchListener {

	public Paddle(Context context, int displayWidth, int displayHeight,
			int paddleSize) {
		super(context);
		this.setOnTouchListener(this);
		_paddleSize = paddleSize;
		_params = new FrameLayout.LayoutParams(_paddleSize, _paddleSize * 2);
		_displayHeight = displayHeight;
		_displayWidth = displayWidth;
		_params.gravity = Gravity.TOP;
		super.setBackgroundResource(R.drawable.fence);
		this.setLayoutParams(_params);

	}

	public int getSize() {
		return _paddleSize;
	}

	public int getPosition() {
		return _params.topMargin;
	}

	public void setPosition(int x, int y) {
		_params.leftMargin = x;
		_params.topMargin = y;
		this.setLayoutParams(_params);
	}

	public boolean onTouch(View v, MotionEvent event) {
		if (MotionEvent.ACTION_DOWN == event.getAction()) {
			y = event.getY();
		} else if (MotionEvent.ACTION_MOVE == event.getAction()) {
			y2 = event.getY();
			yDelta = (int) ((y - y2) + (float) .5);
			_params.topMargin -= yDelta;
			if (_params.topMargin <= 0) {
				_params.topMargin = 0;
			}
			if (_params.topMargin >= _displayHeight - _paddleSize * 2) {
				_params.topMargin = _displayHeight - _paddleSize * 2;
			}
			v.setLayoutParams(_params);
		} else if (MotionEvent.ACTION_UP == event.getAction()) {
			return true;
		}
		return true;
	}

	public int getx() {
		return _params.leftMargin;
	}

	private int _paddleSize;
	private int _displayHeight;
	@SuppressWarnings("unused")
	private int _displayWidth;
	private float y;
	private float y2;
	private int yDelta;
	private FrameLayout.LayoutParams _params;
}
