package com.example.catpawng;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class Ball extends TextView implements View.OnTouchListener {

	public Ball(Context context, int displayWidth, int displayHeight,
			int ballSize) {
		super(context);
		super.setBackgroundResource(R.drawable.cat);
		this.setOnTouchListener(this);

		setSize(ballSize);
		_xDirection = 0;
		_yDirection = 0;

		_params = new FrameLayout.LayoutParams(_ballSize, _ballSize);
		_displayWidth = displayWidth;
		_displayHeight = displayHeight;
		_params.gravity = Gravity.TOP;

		this.setLayoutParams(_params);

	}

	public void updatePosition() {

		// Going right
		if (_xDirection == 1) {
			_params.leftMargin += _ballSpeed;
		}
		// Going left
		else {
			_params.leftMargin -= _ballSpeed;
		}

		// Going down
		if (_yDirection == 1) {
			_params.topMargin += _ballSpeed;

			// Hit bottom
			if (_params.topMargin >= _displayHeight - _ballSize) {
				_yDirection = 0;
				_params.topMargin = _displayHeight - _ballSize;
				// _player.start();
			}

		}
		// Going up
		else {
			_params.topMargin -= _ballSpeed;

			// Hit top
			if (_params.topMargin < 0) {
				_yDirection = 1;
				_params.topMargin = 0;
				// _player.start();
			}
		}
		_event = null;
		onTouch(this, _event);

	}

	public void setPosition(int x, int y) {
		_params.leftMargin = x;
		_params.topMargin = y;

	}
	
	public void setyDirection(int direction) {
		if (direction >=0 && direction <=1 ) _yDirection = direction;
		
	}

	public void setxDirection(int direction) {
		if (direction >=0 &&  direction <= 1)_xDirection = direction;
	}

	public void setSpeed(int speed) {
		if (speed > 0 && speed <= 8) {
			_ballSpeed = speed;
		}
	}

	public void setSize(int x) {

		if (x >= _displayWidth / 32 && x <= _displayWidth / 8) {
			_params.height = x;
			_params.width = x;
			_ballSize = x;

		}

	}

	public int getx() {
		return _params.leftMargin;
	}

	public int gety() {
		return _params.topMargin;
	}
	
	public int getyDirection(){
		return _yDirection;
	}

	public int getSize() {
		return _ballSize;
	}

	public int getSpeed() {
		return _ballSpeed;
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		arg0.setLayoutParams(_params);
		return false;
	}

	private MotionEvent _event;
	private int _xDirection;
	private int _yDirection;
	private FrameLayout.LayoutParams _params;
	private int _displayWidth;
	private int _displayHeight;
	private int _ballSpeed;
	private int _ballSize;

}
