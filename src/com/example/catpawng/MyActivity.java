package com.example.catpawng;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MyActivity extends Activity {
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// FullScreen and locked to landscape
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);

		_ball_handle = new Handler();
		_playArea = (FrameLayout) findViewById(R.id.playArea);

		_button = (ToggleButton) findViewById(R.id.toggleButton1);
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/catty.ttf");
		_button.setTypeface(face);

		// Grabbing Display Size
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		_displayWidth = dm.widthPixels;
		_displayHeight = dm.heightPixels;

		/* GAME SETTINGS */
		_button.setTextSize(_displayHeight / 20);
		_ballSize = _displayWidth / 10;
		_paddleSize = _displayWidth / 16;
		_ballSpeed = 1;
		_player = MediaPlayer.create(this, R.raw.kittycry);

		gameSetup();

	}

	Runnable ball_thread = new Runnable() {
		@Override
		public void run() {
			_ball_handle.post(ball_thread);
			_ball.updatePosition();

			// Hit left paddle
			if (_ball.getx() <= _paddle1.getSize() + _ballSpeed) {
				didCollide(_ball, _paddle1);
			}

			// Hit right paddle
			if (_ball.getx() + _ball.getSize() >= _displayWidth
					- _paddle2.getSize()) {
				didCollide(_ball, _paddle2);
			}

			// Right paddle scored
			if (_ball.getx() <= 0) {
				resetBall();
				playerScore("player2");

			}

			// Left paddle scored
			if (_ball.getx() + _ball.getSize() >= _displayWidth) {
				resetBall();
				playerScore("player1");

			}

		}
	};

	void startBallRunnable() {
		ball_thread.run();
	}

	public void onToggleClicked(View view) {
		boolean on = ((ToggleButton) view).isChecked();

		if (on) {
			startBallRunnable();
		} else {
			stopBall();
		}
	}

	void stopBall() {
		_ball_handle.removeCallbacks(ball_thread);
	}

	private boolean didCollide(Ball ball, Paddle paddle) {
		// Left Paddle
		if (paddle.getx() == 0) {

			for (int i = _paddle1.getPosition(); i <= _paddle1.getPosition()
					+ _paddle1.getSize() * 2; i++) {
				if (// i == _ball.gety() || i == _ball.gety() + _ball.getSize()

				/* || */i == (_ball.gety() + (_ball.gety() + _ball.getSize())) / 2) {

					_ball.setxDirection(1);

					// Hit top of paddle, bounce the y
					if (i < _paddle1.getPosition() + _paddle1.getSize()) {
						if (_ball.getyDirection() == 1)
							_ball.setyDirection(0);
					} else
						_ball.setyDirection(1);
					_ball.setSpeed(_ball.getSpeed() + 1);
					_ball.updatePosition();
					_ball.setSize(_ball.getSize() - 4);
					return true;
				}

			}

		}

		else {
			// Hit right paddle

			for (int i = _paddle2.getPosition(); i <= _paddle2.getPosition()
					+ _paddle2.getSize() * 2; i++) {
				if (// i == _ball.gety() || i == _ball.gety() + _ball.getSize()

				/* || */i == (_ball.gety() + (_ball.gety() + _ball.getSize())) / 2) {
					_ball.setxDirection(0);
					if (i < _paddle1.getPosition() + _paddle1.getSize()) {
						if (_ball.getyDirection() == 1)
							_ball.setyDirection(0);
					} else
						_ball.setyDirection(1);
					_ball.setSpeed(_ball.getSpeed() + 1);
					_ball.updatePosition();
					_ball.setSize(_ball.getSize() - 4);

					return true;
				}
			}

		}

		return false;

	}

	public void gameSetup() {

		_paddle1 = new Paddle(this, _displayWidth, _displayHeight, _paddleSize);
		_paddle1.setPosition(0, _displayHeight / 2 - _paddle1.getSize());

		_paddle2 = new Paddle(this, _displayWidth, _displayHeight, _paddleSize);
		_paddle2.setPosition(_displayWidth - _paddle2.getSize(), _displayHeight
				/ 2 - _paddle2.getSize());

		_ball = new Ball(this, _displayWidth, _displayHeight, _ballSize);
		resetBall();

		_playArea.addView(_ball);
		_playArea.addView(_paddle1);
		_playArea.addView(_paddle2);
	}

	public void resetBall() {
		_ball.setSize(_ballSize);
		_ball.setSpeed(_ballSpeed);
		_ball.setPosition(_displayWidth / 2 - _ball.getSize() / 2,
				_displayHeight / 2 - _ball.getSize() / 2);

	}

	public void onPause() {
		super.onPause();
		_button.setChecked(false);
		stopBall();
	}

	public void onResume() {
		super.onResume();
	}

	public void playerScore(String playerPoint) {
		if (playerPoint == "player1") {
			_player1Score++;
			TextView playerWinner = (TextView) findViewById(R.id.player1score);
			playerWinner.setText("Player 1 Score: " + _player1Score);
		}
		if (playerPoint == "player2") {
			_player2Score++;
			TextView playerWinner = (TextView) findViewById(R.id.player2score);
			playerWinner.setText("Player 2 Score: " + _player2Score);
		}

		_player.start();

	}

	private FrameLayout _playArea;

	private MediaPlayer _player;

	private Handler _ball_handle;
	private Ball _ball;
	private int _ballSize;
	private int _ballSpeed;

	private Paddle _paddle1;
	private Paddle _paddle2;
	private int _paddleSize;

	private ToggleButton _button;

	private int _displayWidth;
	private int _displayHeight;

	private int _player1Score;
	private int _player2Score;

}
